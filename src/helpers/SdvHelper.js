// This example is compatible with any ShapeDiver model ticket.

// ShapeDiver Viewer Initialisation
const initSdvApp = function(container, divParams, ticket) {
  const globalDiv = divParams.current
  // Settings can be defined here, or as attributes of the viewport container. Settings defined here take precedence.
  const settings = {
    container,
    ticket: ticket,
    modelViewUrl: 'eu-central-1',
    showControlsInitial: true,
    showSettingsInitial: false
  }
  // See https://viewer.shapediver.com/v2/2.14.0/doc/SDVApp.ParametricViewer.html for all settings available via the constructor.
  const api = new SDVApp.ParametricViewer(settings)
  let viewerInit = false
  let parameters
  // VISIBILITY_ON is triggered after each scene update. We wait for the first time it is triggered to make sure the scene and parameters are ready.
  api.scene.addEventListener(api.scene.EVENTTYPE.VISIBILITY_ON, function() {
    if (!viewerInit) {
      parameters = api.parameters.get()
      parameters.data.sort(function(a, b) {
        return a.order - b.order
      })
      for (let i = 0; i < parameters.data.length; i++) {
        let paramInput = null
        let paramDiv = document.createElement('div')
        let param = parameters.data[i]
        let label = document.createElement('label')
        label.setAttribute('for', param.id)
        label.innerHTML = param.name
        switch (param.type) {
          case 'Int' || 'Float' || 'Even' || 'Odd':
            paramInput = createRange(param, paramInput)
            break
          case 'Bool':
            paramInput = createCheckbox(param, paramInput)
            break
          case 'Color':
            paramInput = createColorInput(param, paramInput)
            break
          case 'String':
            paramInput = createTextInput(param, paramInput)
            break
          case 'StringList':
            paramInput = createSelect(param, paramInput)
            break
          default:
            break
        }
        if (param.hidden) paramDiv.setAttribute('hidden', '')

        if (param.type === 'Bool') {
          let labelWrap = document.createElement('label')
          labelWrap.classList.add('container')
          let span = document.createElement('span')
          span.classList.add('checkmark')
          labelWrap.append(paramInput)
          labelWrap.append(span)
          paramDiv.appendChild(label)
          paramDiv.appendChild(labelWrap)
          globalDiv.appendChild(paramDiv)
        } else {
          paramDiv.appendChild(label)
          paramDiv.appendChild(paramInput)
          globalDiv.appendChild(paramDiv)
        }
      }

      let exports = api.exports.get()
      for (let i = 0; i < exports.data.length; i++) {
        let exportAsset = exports.data[i]
        let exportDiv = document.createElement('div')
        let exportInput = document.createElement('input')
        exportInput.setAttribute('id', exportAsset.id)
        exportInput.setAttribute('type', 'button')
        exportInput.setAttribute('name', exportAsset.name)
        exportInput.setAttribute('value', exportAsset.name)
        exportInput.onclick = function() {
          api.exports
            .requestAsync({
              id: this.id
            })
            .then(function(response) {
              let link = response.data.content[0].href
              window.location = link
            })
        }
        exportDiv.appendChild(exportInput)
        globalDiv.appendChild(exportDiv)
      }
      viewerInit = true
    }
  })

  const createTextInput = function(param, paramInput) {
    paramInput = document.createElement('input')
    paramInput.setAttribute('id', param.id)
    paramInput.setAttribute('type', 'text')
    paramInput.setAttribute('value', param.value)
    paramInput.onchange = function() {
      api.parameters.updateAsync({
        id: param.id,
        value: this.value
      })
    }
    return paramInput
  }

  const createSelect = function(param, paramInput) {
    paramInput = document.createElement('select')
    paramInput.setAttribute('id', param.id)
    paramInput.classList.add('select-css')
    for (let j = 0; j < param.choices.length; j++) {
      let option = document.createElement('option')
      option.setAttribute('value', j)
      option.setAttribute('name', param.choices[j])
      option.innerHTML = param.choices[j]
      if (param.value === j) option.setAttribute('selected', '')
      paramInput.appendChild(option)
    }
    paramInput.onchange = function() {
      api.parameters.updateAsync({
        id: param.id,
        value: this.value
      })
    }
    return paramInput
  }

  const createColorInput = function(param, paramInput) {
    paramInput = document.createElement('input')
    paramInput.setAttribute('id', param.id)
    paramInput.setAttribute('type', 'color')
    paramInput.setAttribute('value', param.value)
    paramInput.onchange = function() {
      api.parameters.updateAsync({
        id: param.id,
        value: this.value
      })
    }
    return paramInput
  }

  const createCheckbox = function(param, paramInput) {
    paramInput = document.createElement('input')
    paramInput.setAttribute('id', param.id)
    paramInput.classList.add('css-checkbox')
    paramInput.setAttribute('type', 'checkbox')
    paramInput.setAttribute('checked', param.value)
    paramInput.onchange = function() {
      api.parameters.updateAsync({
        id: param.id,
        value: this.checked
      })
    }
    return paramInput
  }

  const createRange = function(param, paramInput) {
    paramInput = document.createElement('input')
    paramInput.setAttribute('id', param.id)
    paramInput.setAttribute('type', 'range')
    paramInput.setAttribute('min', param.min)
    paramInput.setAttribute('max', param.max)
    paramInput.setAttribute('value', param.value)
    if (param.type === 'Int') paramInput.setAttribute('step', 1)
    else if (param.type === 'Even' || param.type === 'Odd') paramInput.setAttribute('step', 2)
    else paramInput.setAttribute('step', 1 / Math.pow(10, param.decimalplaces))
    paramInput.onchange = function() {
      api.parameters.updateAsync({
        id: param.id,
        value: this.value
      })
    }
    return paramInput
  }
}

export default initSdvApp
