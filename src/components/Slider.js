import React, { Component } from 'react'
import ProductItem from '../components/ProductItem'
import SliderSlick from 'react-slick'

class Slider extends Component {
  arrowNext() {
    return (
      <svg width="8" height="28" viewBox="0 0 8 28" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M0 6.44961L12.9032 0L25.8065 6.44961"
          transform="translate(7 1.00001) rotate(90)"
          stroke="#6E6E6E"
        />
      </svg>
    )
  }

  arrowPrev() {
    return (
      <svg width="8" height="28" viewBox="0 0 8 28" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M0 6.44961L12.9032 0L25.8065 6.44961"
          transform="translate(1 27) rotate(-90)"
          stroke="#6E6E6E"
        />
      </svg>
    )
  }

  render() {
    const settings = {
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      vertical: false,
      prevArrow: this.arrowPrev(),
      nextArrow: this.arrowNext(),
      responsive: [
        {
          breakpoint: 1199,
          settings: {
            slidesToShow: 3
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    }

    return (
      <div className="same-prod-slider-box">
        <div className="same-prod-slider js-same-prod-slider">
          <SliderSlick {...settings}>
            <div>
              <ProductItem />
            </div>
            <div>
              <ProductItem />
            </div>
            <div>
              <ProductItem />
            </div>
            <div>
              <ProductItem />
            </div>
            <div>
              <ProductItem />
            </div>
            <div>
              <ProductItem />
            </div>
          </SliderSlick>
        </div>
      </div>
    )
  }
}

export default Slider
