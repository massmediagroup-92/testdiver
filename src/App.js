import React from 'react'
import './styles/styles.scss'
import Navbar from './components/Navbar'
import ProductCard from './components/ProductCard'
import Footer from './components/Footer'

function App() {
  return (
    <div className="app">
      <Navbar />
      <ProductCard />
      <Footer />
    </div>
  )
}

export default App
